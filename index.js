let Parser = require('rss-parser');
let parser = new Parser();
let storage = require('node-persist');
const { get } = require('node-persist');

const feedURL = "https://nitter.net/stoerungsmelder/rss";
const feedWithImages = "https://nitter.net/WorldPressPhoto/rss";
//const pastIncidentTime =

async function getFeed(URL) {
	return await parser.parseURL(URL);
}

async function extractNewPosts (Feed) {
	var lastPostTime = await storage.getItem('lastPostTime');
	var i = 0;
	var currentPosts = {
		items: []
	};
	while (Date.parse(Feed.items[i] && Feed.items[i].isoDate) >= lastPostTime) {
		currentPosts.items[i] = {};
		currentPosts.items[i].content = Feed.items[i].content;
		currentPosts.items[i].isoDate = Feed.items[i].isoDate;
		i++;
	}
	return currentPosts;
}

(async () => {
	await storage.init();
	if (isNaN(storage.getItem('lastPostTime'))) {
		await storage.setItem('lastPostTime', '0')
	};
	const Feed = await getFeed(feedURL).catch(error => { throw new Error('could not get Feed') });
	var newPosts = await extractNewPosts(Feed);
})()
